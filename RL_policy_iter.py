import numpy as np
import pandas as pd
import time


class rlalgorithm:
    def __init__(self, actions, env, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.v_table = {}
        self.pie_table = {}
        self.display_name = "Value Iter"
        self.init_all_state(env)
        t = time.time()
        self.policy_iter(env)
        elapsed = time.time() - t
        print("policy iter",elapsed)

    '''Choose the next action to take given the observed state'''
    def choose_action(self, observation):
        self.check_state_exist(observation)
        action = self.pie_table[observation]
        return action

    def init_all_state(self, env):
        s = env.reset(value=0)
        self.dfs(s, env)

    def dfs(self, s, env):
        if str(s) in self.v_table:
            return
        self.check_state_exist(str(s))
        for action in self.actions:
            new_state = self.step(s, action, env)
            self.dfs(new_state, env)

    def step(self, s, action, env):
        base_action = np.array([0, 0])
        if action == 0:   # up
            if s[1] > env.UNIT:
                base_action[1] -= env.UNIT
        elif action == 1:   # down
            if s[1] < (env.MAZE_H - 1) * env.UNIT:
                base_action[1] += env.UNIT
        elif action == 2:   # right
            if s[0] < (env.MAZE_W - 1) * env.UNIT:
                base_action[0] += env.UNIT
        elif action == 3:   # left
            if s[0] > env.UNIT:
                base_action[0] -= env.UNIT
        new_state = [s[0]+base_action[0], s[1]+base_action[1],
                     s[2]+base_action[0], s[3]+base_action[1]]
        return new_state

    def computeReward(self, currstate, action, nextstate, env):
        reverse = False
        if nextstate == env.canvas.coords(env.goal):
            reward = 1
            done = True
            nextstate = 'terminal'
        elif nextstate in [env.canvas.coords(w) for w in env.wallblocks]:
            reward = -0.3
            done = False
            nextstate = currstate
            reverse = True
        elif nextstate in [env.canvas.coords(w) for w in env.pitblocks]:
            reward = -10
            done = True
            nextstate = 'terminal'
            reverse = False
        else:
            reward = -0.1
            done = False
        return reward, done, reverse, nextstate

    def policy_eval(self, env, epsilon=0.005):
        iter = 0
        while True:
            delta = 0
            for state in self.v_table.keys():
                #if state == "[85.0, 85.0, 115.0, 115.0]":
                #    print("+")
                s = list(map(float, state.strip('][').split(', ')))
                if s in [env.canvas.coords(w) for w in env.pitblocks] or s in [env.canvas.coords(w) for w in env.wallblocks] or s == env.canvas.coords(env.goal):
                    continue

                i = self.pie_table[state]
                new_s = self.step(s, i, env)
                reward, done, reverse, new_s = self.computeReward(s, i, new_s, env)
                if not done:
                    reward += self.gamma * self.v_table[str(new_s)]
                delta = max(delta,abs(self.v_table[str(s)] - reward))
                self.v_table[str(s)] = reward
            iter += 1
            print("iter: "+str(iter))
            if delta < epsilon:
                return
            if iter > 50:
                return 

    def policy_improve(self,env):
        policy_stable = True
        for state in self.v_table.keys(): 
            old_action = self.pie_table[state]
            s = list(map(float, state.strip('][').split(', ')))
            if s in [env.canvas.coords(w) for w in env.pitblocks] or s in [env.canvas.coords(w) for w in env.wallblocks] or s == env.canvas.coords(env.goal):
                continue
            rewards = [0]*len(self.actions)
            for i in list(range(len(self.actions))):
                new_s = self.step(s, i, env)
                reward, done, reverse, new_s = self.computeReward(s, i, new_s, env)
                if not done:
                    reward += self.gamma * self.v_table[str(new_s)]
                rewards[i] = reward

            self.pie_table[state] = rewards.index(max(rewards))
            if old_action != self.pie_table[state]:
                policy_stable = False
        return policy_stable

    def policy_iter(self,env):
        stable = False
        counter = 0
        while not stable:
            print(str(counter)+"-th policy iteration")
            self.policy_eval(env)
            stable = self.policy_improve(env)
            counter += 1

    def learn(self, s, a, r, s_):
        # Don't really do anything since we already figure out the best scenario
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
        else:
            v_target = r  # next state is terminal
        return s_, a_
    
    '''States are dynamically added to the Q(S,A) table as they are encountered'''

    def check_state_exist(self, state):
        if state in self.v_table:
            return True

        # append new state to q table
        self.v_table[state]=0
        self.pie_table[state]=0
        return False
